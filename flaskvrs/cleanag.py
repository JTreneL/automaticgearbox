import sys
import threading
import web
import time


motor = {
    "rpm":0, 
    "speed":0, 
    "gear":"N"
}


def enginestart(motor):
    if motor["rpm"] == 0 and motor["speed"] == 0:
        motor["rpm"] = 900
        automaticgearbox(motor)
    return motor
    

def engineoff(motor):
    if motor["rpm"] == 900 and motor["speed"] == 0:
        motor["rpm"] = 0
        automaticgearbox(motor)
    return motor


def gas(motor):
    if motor["speed"] <= 220 and motor["rpm"] > 0:
        motor["speed"] += 5
        motor["rpm"] += 400
        automaticgearbox(motor)
        return motor
    else:
        return motor


def brake(motor):
    if motor["rpm"] > 0 and motor["speed"] > 0:
        motor["speed"] -= 5
        motor["rpm"] -= 400
        automaticgearbox(motor)
        return motor
    elif motor["speed"] == 0:
        return motor
    else:
        motor["speed"] -= 5
        motor["rpm"] = 0
        automaticgearbox(motor)
        return motor


def automaticgearbox(motor):
    if motor["rpm"] <= 900 and motor["speed"] >= 0:
        motor["gear"] = "N"
        return motor
    elif motor["rpm"] > 900 and motor["speed"] < 10:
        motor["gear"] = 1
        return motor
    elif motor["rpm"] > 900 and motor["speed"] < 20:
        motor["gear"] = 2
        return motor
    elif motor["rpm"] > 900 and motor["speed"] < 40:
        motor["gear"] = 3
        return motor
    elif motor["rpm"] > 900 and motor["speed"] < 60:
        motor["gear"] = 4
        return motor
    elif motor["rpm"] > 900 and motor["speed"] < 90:
        motor["gear"] = 5
        return motor
    elif motor["rpm"] > 900 and motor["speed"] < 120:
        motor["gear"] = 6
        return motor
    elif motor["rpm"] > 900 and motor["speed"] < 150:
        motor["gear"] = 7
        return motor
    elif motor["rpm"] > 900 and motor["speed"] < 220:
        motor["gear"] = 8
        return motor


def main():
    port = 5000
    run_web = threading.Thread(target=web.start, args=(port,))
    run_web.start()
    while True:
            try:
                time.sleep(1000)
            except KeyboardInterrupt: 
                break
    web.stop()
    

if __name__ == "__main__":
    sys.exit(main())

