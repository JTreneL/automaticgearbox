import flask
from flask import render_template, request
import os
import sys
import cleanag
import json

basedir = os.path.dirname(sys.executable if getattr(sys, 'frozen', False) else __file__)
app = flask.Flask(__name__, template_folder=os.path.join(basedir, 'templates'), static_folder=os.path.join(basedir, 'static'),)
srv = None
motor = cleanag.motor


@app.route("/")
def motorstatus():
   return render_template("index.html", rpm=motor.get("rpm"), speed=motor.get("speed"), gear=motor.get("gear"))
   
@app.route('/on', methods=['GET', 'POST'])
def enginestart():
   global motor
   flask.jsonify(cleanag.enginestart(motor))
   return render_template("index.html", rpm=motor.get("rpm"), speed=motor.get("speed"), gear=motor.get("gear"))

@app.route('/gas', methods=['GET', 'POST'])
def gas():
   global motor
   flask.jsonify(cleanag.gas(motor))
   return render_template("index.html", rpm=motor.get("rpm"), speed=motor.get("speed"), gear=motor.get("gear"))

@app.route('/off', methods=['GET', 'POST'])
def engineoff():
   global motor
   flask.jsonify(cleanag.engineoff(motor)) 
   return render_template("index.html", rpm=motor.get("rpm"), speed=motor.get("speed"), gear=motor.get("gear"))

@app.route('/brake', methods=['GET', 'POST'])
def brake():
   global motor
   flask.jsonify(cleanag.brake(motor))
   return render_template("index.html", rpm=motor.get("rpm"), speed=motor.get("speed"), gear=motor.get("gear"))

def start(port_):
   app.run("localhost")


def stop():
   global srv
   if srv:
       srv._do_serve = 0

# Pošle požadavek na stránku.
#curl -X POST http://localhost:5000/gas -d 'motor={"speed": 0, "rpm": 900, "gear": 1}'
