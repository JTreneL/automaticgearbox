import sys
rpm = 0
speed = 0
gear = "N"

def gas(speed,rpm,gear):
    while speed < 220:
        speed += 5
        rpm += 400
        motor(rpm,speed,gear)
    else:
        print("Maximum RPM and Speed!")
        motor(rpm,speed,gear)


def gashard(speed,rpm,gear):
    while speed < 220:
        speed += 10
        rpm += 800
        motor(rpm,speed,gear)
    else:
        print("Maximum RPM and Speed!")
        motor(rpm,speed,gear)


def brake(speed,rpm,gear):
    while rpm > 0:
        speed -= 5
        rpm -= 400
        motor(rpm,speed,gear)
    else:
        speed -= 5
        rpm = 0
        motor(rpm,speed,gear)


def brakehard(speed,rpm,gear):
    while speed > 5 and rpm > 0:
        speed -= 10
        rpm -= 800
        motor(rpm,speed,gear)
    else:
        brake(speed,rpm,gear)
        

def enginestart(rpm,speed,gear):
    print("Starting Engine...")
    rpm = 900 
    motor(rpm,speed,gear)
    

def engineoff(rpm,speed,gear):
    print("Stoping Engine...")
    rpm = 0
    motor(rpm,speed,gear)


def automaticgearbox(rpm,speed,gear):
    gears = ["R","N",1,2,3,4,5,6,7,8]
    while speed < 220:
        if rpm <= 900 and speed >= 0:
            gear = (gears[1])
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
        elif rpm > 900 and speed < 10:
            gear = (gears[2])
            rpm -= 200
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
        elif rpm > 900 and speed < 20:
            gear = (gears[3])
            rpm -= 200
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
        elif rpm > 900 and speed < 40:
            gear = (gears[4])
            rpm -= 1000
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
        elif rpm > 900 and speed < 60:
            gear = (gears[5])
            rpm -= 2600
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
        elif rpm > 900 and speed < 90:
            gear = (gears[6])
            rpm -= 4200
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
        elif rpm > 900 and speed < 120:
            gear = (gears[7])
            rpm -= 6600
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
        elif rpm > 900 and speed < 150:
            gear = (gears[8])
            rpm -= 9000
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
        elif rpm > 900 and speed < 220:
            gear = (gears[9])
            rpm -= 11400
            return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed
    else:
        gear = (gears[9])
        rpm = 7100
        speed = 220
        return "RPM:",rpm,"GEAR:",gear,"SPEED:",speed


def motor(rpm,speed,gear):
    x = 0
    while x < 1:
        info = automaticgearbox(rpm,speed,gear)
        print(info[0])
        print(info[1])
        print(info[2])
        print(info[3])
        print(info[4])
        print(info[5])
        startoff = input("1.[Start] 2.[Off] 3.[GAS] 4.[GASHARD] 5.[BRAKE] 6.[BRAKEHARD] ")
        if startoff == "1":
            while rpm == 0:
                enginestart(rpm,speed,gear)
            else:
                print("Engine is running!")
        elif startoff == "2":
            while rpm > 0:
                engineoff(rpm,speed,gear)
            else: 
                print("Engine is not running!")
        elif startoff == "3":
            while rpm > 0:
                gas(speed,rpm,gear)
            else: 
                print("Engine is dead")
        elif startoff == "4":
            while rpm > 0:
                gashard(speed,rpm,gear)
            else: 
                print("Engine is dead")
        elif startoff == "5":
            while speed > 0:
                brake(speed,rpm,gear)
            else: 
                print("Car is dont moving.")
        elif startoff == "6":
            while speed > 0:
                brakehard(speed,rpm,gear)
            else: 
                print("Car is dont moving.")

    
if __name__ == "__main__":
    sys.exit(motor(rpm,speed,gear))