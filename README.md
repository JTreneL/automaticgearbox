# Automatic Gearbox: AG 

- Automatic transmission that allows you to change gears N-8 depending on engine speed.

<img src="./example.png">

# Specification
- Maximum speed == 220 km/h
- Maximum rpm == Who know! 

# Functions:
- Gas/Gashard
- Brake/Brakehard
- On 
- Off 
- Warning when engine on/off 
- Warning when engine exceed maximum speed and rpm 

# Technology:
- Flask
- Json
