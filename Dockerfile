FROM python:3.10-alpine
RUN apk --update add gcc build-base freetype-dev libpng-dev openblas-dev
RUN mkdir home/automaticgearbox
WORKDIR home/automaticgearbox
COPY . .
ENTRYPOINT ["python", "ag.py"]
